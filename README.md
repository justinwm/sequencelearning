# README #

This repository provides details to replicate the experiments reported in the paper "How Well Do Change Sequences Predict Defects" submitted to TSE.

##./Datasets_SequenceLearning##

This folder contains the data for each project. Specifically, each subfolder under ./Datasets_SequenceLearning represents a project.

For the subfolder of each project, there are multiple files stores the information of this project.

* authorindex.txt: indexes all the developers for the project
* commitAuthor.txt: stores the developer of each commit
* commitFiles.txt: stores the modified source files of each commit
* commitTime.txt: stores the committed time for each commit
* hunkIndex.txt: indexes all the hunks for the project
* sourceFilesIndex.txt: indexes all the source files of the project

There are multiple subfolders in the folder of each project, and each of them represents an version of the project.

For example, the folder of ./Datasets_SequenceLearning/ant/1.5 stores the information of project Ant of version 1.5.
Specifically:

* mo_metrics.csv: stores all the metrics for the feature set MO
* rh_metrics.csv: stores all the metrics for the feature set RH
* sc_metrics.csv: stores all the metrics for the feature set SCC
* ck_oo.csv: stores all the metrics for the feature set of static code metrics
* sourceFileSequence.txt: stores the change sequences of for each source file in the version


##./Traditional##

This folder contains the source code to generate the results of traditional process-metrics based approaches

* `./Traditional/lib:` contains the required libraries such as `weka.jar`.
* `SequenceLearning.jar`: contains the implementations of the baselines.

To generate the results of RQ1, you can run our implementations using the following command:

`java -cp ./lib/weka.jar:./SequenceLearning.jar main.Main -task RQ1`

Similary, to generate the results of RQ3, you can run the following command:

`java -cp ./lib/weka.jar:./SequenceLearning.jar main.Main -task RQ3`


##./Sequence##

This folder contains the implementation to generate the results of change sequence based defect prediction approaches

* `./tensorflow:` contains the implementations of RNN of Fences based on `tensorflow`
* `./tfdata:` contains the sequence data for each project, for example, `./tfdata/ant` contains the sequence data of project Ant. 

Under the data folder of each project `./tfdata/ant`, there are multiple files ended with `.pkl`, which can be recognized by tensorflow

* `./tfdata/ant/1.6_all.pkl` combines all the six types of sequences together for version 1.6
* `./tfdata/ant/1.6_semantic.pkl` contains the change sequences of type `semantics`, and the other five types of sequences are ended with their names, such as `1.6_type.pkl`, `1.6_intervel.pkl` and so on.

To run Fences, go to folder `tensorflow` first, and run the following command:

`python2.7 RNN.py xalan 2.5 all 0.1 100 128`

Noted that you need to install tensorflow first in your environment (https://www.tensorflow.org/install/install_linux). `xalan` refer to the project. `2.5` refers to the prediction version. `all` refers to the types of sequences, there are seven choices (all, interval, authorship, cochange, codechurn, semantic and type). `0.1` refers to the learning rate. `100` refers to the iteration numbers and `128` refers to the number of hidden layers.

Following the above command, you can run the experiment of other project.


##./Transfer##

This folder contains the implementation to generate the `.pkl` files in the second step.
First, we need to generate the raw data of all sequences.
To generate the sequences for project `camel`, you can simply run the following commands.

`java -jar SequenceExtraction.jar -p camel`

All the sequence files are generated in the folder `./Datasets_Sequencelearning/camel`

We need to copy all the generated raw sequences to the `../Sequence/tfdata` folder in order to generate the `pkl` file.

Specifically, we run the following two commands:

`cp ../Datasets_SequenceLearning/camel/*_test.txt ../Sequence/tfdata/camel/`
`cp ../Datasets_SequenceLearning/camel/*_train.txt ../Sequence/tfdata/camel/`

Aftering copying all the files to the target location, we can generate the `pkl` files for each type of sequences using the following command:

`python2.7 tfdata.py project version type`, similary, type has seven choices (all, interval, authorship, cochange, codechurn, semantic and type).

For example,

`python2.7 tfdata.py camel 1.4 all`

Similar operations need to be conducted in order to generate the `pkl` files.
After the `pkl` files are generated, you can go to the second step to run the LSTM.