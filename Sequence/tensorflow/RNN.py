'''
A Recurrent Neural Network (LSTM) implementation example using TensorFlow library.
This example is using the MNIST database of handwritten digits (http://yann.lecun.com/exdb/mnist/)
Long Short Term Memory paper: http://deeplearning.cs.cmu.edu/pdfs/Hochreiter97_lstm.pdf

'''

from __future__ import print_function

import numpy
import time
import sys
import tensorflow as tf
from tensorflow.contrib import rnn
from tensorflow.examples.tutorials.mnist import input_data
import deepBugPrediction
from itertools import repeat

# Parameters
n_input = 28
n_steps = 28
# Network Parameters
n_hidden = 128
# hidden layer num of features
n_classes = 2  # only two classes (buggy or clean)
learning_rate = 0.1
training_iters = 100
batch_size = 64
display_step = 10

# the input data
data_path = "../tfdata/"
train_x = []
train_y = []
train_len = []
test_x = []
test_y = []
test_len = []
train_x1 = []
train_y1 = []
train_len1 = []

tag = "semantic"


def RNN(x, x_len, weights, biases):
    # Prepare data shape to match `rnn` function requirements
    # Current data input shape: (batch_size, n_steps, n_input)
    # Required shape: 'n_steps' tensors list of shape (batch_size, n_input)

    # Permuting batch_size and n_steps
    x = tf.transpose(x, [0, 1, 2])
    # Reshaping to (n_steps*batch_size, n_input)
    #    x = tf.reshape(x, [-1, n_input])
    # Split to get a list of 'time_step' tensors of shape (batch_size, n_input)
    #    x = tf.split(0, n_steps, x)

    # Define a lstm cell with tensorflow
    lstm_cell = rnn.BasicLSTMCell(n_hidden, forget_bias=1.0)

    # Get lstm cell output

    outputs, states = tf.nn.dynamic_rnn(lstm_cell, x, dtype=tf.float32, sequence_length=x_len)

    #   print(outputs)
    # Linear activation, using rnn inner loop last output

    # only select the last time step
    #    outputs = tf.transpose(outputs, [1, 0, 2])
    #    outputs = tf.gather(outputs, int(outputs.get_shape()[0]) - 1)
    #    prediction = tf.matmul(outputs, weights['out']) + biases['out']
    # mean pooling over the all the time steps
    outputs = tf.reshape(outputs, [-1, n_hidden])
    prediction = tf.matmul(outputs, weights['out']) + biases['out']
    prediction = tf.reshape(prediction, [batch_size, -1, n_classes])
    # prediction = tf.reduce_mean(prediction, 1)
    prediction = tf.reduce_sum(prediction, 1)
    xx_len = tf.tile(x_len, [n_classes])
    xx_len = tf.reshape(xx_len, [-1, n_classes])
    prediction = tf.truediv(prediction, tf.cast(xx_len, tf.float32))
    return prediction


def fmeasure(pred, y, len):
    predict = tf.argmax(pred, 1)
    label = tf.argmax(y, 1)
    fake = tf.tile([1], [len])
    x_predict = tf.equal(tf.cast(predict, tf.int32), tf.cast(fake, tf.int32))
    x_label = tf.equal(tf.cast(label, tf.int32), tf.cast(fake, tf.int32))
    true_positive = tf.logical_and(x_predict, x_label)
    xor = tf.logical_xor(x_predict, x_label)
    false_positive = tf.logical_and(xor, x_predict)
    false_negative = tf.logical_and(xor, x_label)
    true_positive = tf.cast(true_positive, tf.float32)
    false_positive = tf.cast(false_positive, tf.float32)
    false_negative = tf.cast(false_negative, tf.float32)
    precision = tf.div(tf.reduce_sum(true_positive),
                       tf.add(tf.reduce_sum(true_positive), tf.reduce_sum(false_positive)))
    recall = tf.div(tf.reduce_sum(true_positive), tf.add(tf.reduce_sum(true_positive), tf.reduce_sum(false_negative)))
    return tf.div(tf.multiply(precision, recall), tf.add(precision, recall))


def run():
    # tf Graph input
    x = tf.placeholder("float", [batch_size, None, n_input])
    y = tf.placeholder("float", [None, n_classes])
    x_len = tf.placeholder("int32", [None])
    # Define weights
    weights = {
        'out': tf.Variable(tf.random_normal([n_hidden, n_classes]))
    }
    biases = {
        'out': tf.Variable(tf.random_normal([n_classes]))
    }

    pred = RNN(x, x_len, weights, biases)

    # Define loss and optimizer
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
    # fm = fmeasure(pred,y)
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    # Evaluate model
    correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    # Initializing the variables
    init = tf.initialize_all_variables()
    start = 0
    end = batch_size
    # Launch the graph
    with tf.Session() as sess:
        sess.run(init)
        step = 1
        # Keep training until reach max iterations
        beginTime = time.time()
        while step < training_iters:
            batch_x = train_x[start:end]
            batch_y = train_y[start:end]
            batch_len = train_len[start:end]
            start += batch_size
            end += batch_size
            if start >= len(train_x):
                step += 1
                start = 0
                end = batch_size
            elif end > len(train_x):
                end = len(train_x)
                start = end - batch_size

            # print(batch_x.shape, "\t", batch_y.shape, "\t", batch_len.shape)

            # Run optimization op (backprop)
            sess.run(optimizer, feed_dict={x: batch_x, y: batch_y, x_len: batch_len})

            if step % display_step == 0:
                # Calculate batch accuracy
                acc = sess.run(accuracy, feed_dict={x: batch_x, y: batch_y, x_len: batch_len})
                # Calculate batch loss
                loss = sess.run(cost, feed_dict={x: batch_x, y: batch_y, x_len: batch_len})
                #   train_predict = sess.run(pred, feed_dict={x: batch_x, y: batch_y, x_len: batch_len})
                #   print(train_predict)

                print("Iter " + str(step) + ", Minibatch Loss= " + \
                      "{:.6f}".format(loss) + ", Training Accuracy= " + \
                      "{:.5f}".format(acc))

        print("Optimization Finished!")
        endTime = time.time()
        test_predict = sess.run(pred, feed_dict={x: test_x[0:batch_size], y: test_y[0:batch_size], x_len: test_len[0:batch_size]})
        # print(test_predict)
        fmtest = fmeasure(test_predict, test_y[0:batch_size], len(test_y[0:batch_size]))
        print("Testing Accuracy:", sess.run(accuracy, feed_dict={x: test_x[0:batch_size], y: test_y[0:batch_size], x_len: test_len[0:batch_size]}))
        filename = data_path + pro + "/" + version + "_" + tag + "_" + str(learning_rate) + "_" + str(
            training_iters) + "_" + str(n_hidden) + "_test_result.txt"
        lines = [str(endTime - beginTime)]
        for line in test_predict:
            lines.append(','.join(map(str, line)))
        with open(filename, 'w') as f:
            f.write('\n'.join(lines))
            f.close()
        fm = fmeasure(test_predict, test_y[0:batch_size], len(test_y[0:batch_size]))
        print("Testing FMeasure:\t", 2 * fm.eval())

        filename = data_path + pro + "/" + version + "_" + tag + "_" + str(learning_rate) + "_" + str(
            training_iters) + "_" + str(n_hidden) + "_train_result.txt"
        train_predict = sess.run(pred, feed_dict={x: train_x1[0:batch_size], y: train_y1[0:batch_size], x_len: train_len1[0:batch_size]})
        fm = fmeasure(train_predict, train_y1[0:batch_size], len(train_y1[0:batch_size]))
        print("Training FMeasure:\t", 2 * fm.eval())

        lines = [str(endTime - beginTime)]
        for line in train_predict:
            lines.append(','.join(map(str, line)))
        with open(filename, 'w') as f:
            f.write('\n'.join(lines))
            f.close()
        save_file = data_path + tag + "_result.txt"
        with open(save_file, 'a') as f:
            f.write(tag + "\t" + pro + "\t" + version + "\t" + str(2 * fmtest.eval()) + "\n")
            f.close()


if __name__ == '__main__':
    pro = sys.argv[1]
    version = sys.argv[2]
    tag = sys.argv[3]
    if len(sys.argv) > 4:
        learning_rate = float(sys.argv[4])
    if len(sys.argv) > 5:
        training_iters = int(sys.argv[5])
    if len(sys.argv) > 6:
        n_hidden = int(sys.argv[6])
    print(pro, "\t", version, "\t" + tag)
    train_x, train_y, train_len, test_x, test_y, test_len, train_x1, train_y1, train_len1 = deepBugPrediction.load_data(
        pro, version, tag)
    print("train len", max(train_len))
    print("test len", max(test_len))
    n_steps = max(max(train_len), max(train_len1))
    test_max = max(test_len)
    n_input = len(train_x[0][0])
    n_classes = len(train_y[0])
    zeros = [0] * n_input
    print(n_steps, "\t", n_input, "\t", n_classes, "\t", len(train_len), "\t", len(test_len), "\t", len(train_len1))
    for item in train_x:
        if len(item) < n_steps:
            item.extend(repeat(zeros, n_steps - len(item)))
    for item in test_x:
        if len(item) < test_max:
            item.extend(repeat(zeros, test_max - len(item)))
    for item in train_x1:
        if len(item) < n_steps:
            item.extend(repeat(zeros, n_steps - len(item)))

    train_x = numpy.array(train_x)
    train_y = numpy.array(train_y)
    print("train shape", train_x.shape)

    train_len = numpy.array(train_len)
    test_x = numpy.array(test_x)
    print("test_shape", test_x.shape)
    test_y = numpy.array(test_y)
    test_len = numpy.array(test_len)
    train_x1 = numpy.array(train_x1)
    train_y1 = numpy.array(train_y1)
    train_len1 = numpy.array(train_len1)
    run()
