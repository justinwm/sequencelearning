#Author: Justin Civi

from __future__ import print_function

from six.moves import xrange
import six.moves.cPickle as pickle
import os
import sys
import numpy
from numpy import size

batch_size = 2
prefix = "../tfdata/"
# Import Data


def load_data(pro, version, tag, valid_portion=0.1):
    filename = prefix + pro + "/" + version + "_" + tag + ".pkl";
    f = open(filename, 'rb')
    train_x, train_y = pickle.load(f)
    test_x, test_y = pickle.load(f)


#   No permutation
    train_size = len(train_y)
    p_index = [i for i in range(train_size) if train_y[i][0] == 0]
    n_index = [i for i in range(train_size) if train_y[i][0] == 1]
    train_index = range(train_size)
    n_index = numpy.random.permutation(n_index)
    train_x_sample = []
    train_y_sample = []
    for i in range(len(p_index)):
        train_x_sample.append(train_x[p_index[i]])
        train_y_sample.append(train_y[p_index[i]])
        if i < len(n_index):
            train_x_sample.append(train_x[n_index[i]])
            train_y_sample.append(train_y[n_index[i]])
    test_len = [len(s) for s in test_x]
    train_len = [len(s) for s in train_x]
    median = max(test_len)
    for index, item in enumerate(train_x):
        if len(item) > median:
            train_x[index] = item[len(item) - median: len(item)]

    for index, item in enumerate(train_x_sample):
        if len(item) > median:
            train_x_sample[index] = item[len(item) - median: len(item)]
    train_len_sample = [len(s) for s in train_x_sample]
    train_len = [len(s) for s in train_x]
    test_len = [len(s) for s in test_x]

    return train_x_sample, train_y_sample, train_len_sample, test_x, test_y, test_len, train_x, train_y, train_len


def main(argv):
    pro = argv[0]
    version = argv[1]
    tag = argv[2]
    load_data(pro,version,tag)

if __name__ == '__main__':
    main(sys.argv[1:])
