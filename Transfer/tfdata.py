dataset_path='../Sequence/tfdata/'

import numpy
import cPickle as pkl
import glob
import os, sys

def build_data(filename):
    data_x = []
    data_y = []
    data_sid = []
    with open (filename,'r') as f:
        for line in f:
            seq = line.split('\t')
            if len(seq) > 2:
                data_sid.append(seq[0])
                if (seq[1] == "1"):
                    data_y.append([0,1])
                else:
                    data_y.append([1,0])
                tensors = []
                for item in seq[2:]:
                    tensors.append(eval(item))
                data_x.append(tensors)
    return data_x, data_y, data_sid


def main(argv):
    pro = argv[0]
    version = argv[1]
    type = argv[2]
    path = dataset_path + pro + "/"
    print(path)
    train_x, train_y, train_sid = build_data(path + version + "_" + type + "_train.txt")
    print(len(train_x), "\t", len(train_y), "\t", len(train_sid))
    test_x, test_y, test_sid = build_data(path + version + "_" + type + "_test.txt")
    print(len(test_x), "\t", len(test_y), "\t", len(test_sid))
    f = open(path + version + "_" + type + ".pkl", 'wb')
    pkl.dump((train_x, train_y), f, -1)
    pkl.dump((test_x, test_y), f, -1)
    f.close()

    filename = path + version + "_" + type + "_test_sid.txt"
    with open(filename, 'w') as f:
        f.write("\n".join(test_sid))
        f.close()

    filename = path + version + "_" + type + "_train_sid.txt"
    with open(filename, 'w') as f:
        f.write("\n".join(train_sid))
        f.close()

if __name__ == '__main__':
    main(sys.argv[1:])
